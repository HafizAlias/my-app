import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet } from 'react-native'

class Todo extends Component {
   state = {
      todo: '',
  
   }
   handletodoinput = (text) => {
      this.setState({ todo: text })
   }
   
   Addtodo = (todo) => {
      alert('Todo Item: ' + todo )
   }
   render() {
      return (

        
         <View style = {styles.container}>

<Text style={styles.appTitle}>Todo App</Text>
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Add your task here..."
               placeholderTextColor = "grey"
               autoCapitalize = "none"
               onChangeText = {this.handletodoinput}/>
            
           
            
            <TouchableOpacity
               style = {styles.submitButton}
               onPress = {
                  () => this.Addtodo(this.state.todo)
               }>
               <Text style = {styles.submitButtonText}> Submit </Text>
            </TouchableOpacity>
         </View>
      )
   }
}
export default Todo

const styles = StyleSheet.create({
   container: {
      paddingTop: 23
   },
   input: {
      margin: 15,
      height: 40,
      borderColor: 'grey',
      borderWidth: 1
   },

   appTitle: {
    textAlign:'center',
    color: 'black',
    fontSize: 25,
    marginTop: 60,
    marginBottom: 30,
   },
   submitButton: {
      backgroundColor: '#5FD65D',
   
      justifyContent: 'center',
      alignItems: 'center',
      margin: 15,
      height: 40,
   },
   submitButtonText:{
      textAlign:'center',
     
      color: 'white'
   }
})