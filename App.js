
import React from 'react';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import { Icon } from 'react-native-elements';
import Todo from './screen/Todo';
import Tasklist from './screen/Tasklist';


  const TabNavigator = createBottomTabNavigator(

    {
      Home: { 
      screen : Todo,
      navigationOptions :{
        tabBarIcon: <Icon name="home" size={21} />
      }
    },

        List: { 
          screen : Tasklist,
          navigationOptions :{
            tabBarIcon: <Icon name="list" size={21} />
          }
    },

      
    },
    {
      tabBarOptions: {
        activeTintColor: 'red',
        inactiveTintColor: 'gray'
        
       
      }
    }
  );
    


  
   
 
  
  



export default createAppContainer(TabNavigator);